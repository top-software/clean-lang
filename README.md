# The Clean programming language

This repository contains the sources for
https://top-software.gitlab.io/clean-lang.

Please contribute!

## Local installation

```bash
sudo pip3 install -r requirements.txt
mkdocs serve
```

You can now open http://localhost:8000/ in your browser.

Alternatively, use `mkdocs build` to generate a static version.

## Authors &amp; License

See [this page](/docs/misc/about.md).
