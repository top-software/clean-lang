---
vim: noexpandtab tabstop=2 shiftwidth=2
---

# The ABC machine in detail

The ABC machine is the abstract machine for which the Clean *compiler*
generates code. The *code generator* can generate actual machine code from ABC
code for various platforms. See [the introduction to the run time
system](introduction.md#the-abc-machine) for a global overview of the ABC
machine. This page contains some more in depth material.

For people working on a low level with Clean, it can be useful to implement
parts of an application in ABC (through
[`code { ... }`](https://cloogle.org/#code) blocks). It is also a convenient
target language if you are implementing a compiler, because it is still
relatively high level and has a code generator that optimises a lot of
inefficient ABC code away.

If you are looking into the ABC machine, the following things may be helpful:

- The ABC machine was originally described by Pieter Koopman in his 1990
	dissertation *Functional Programs as Executable Specifications*. For a port
	of the original *Miranda* implementation to Clean, see this
	[abc-machine](https://git.camilstaps.nl/archived/clean/abc-machine.git/)
	repository. While this dissertation is useful to get an overview of the
	design of the abstract machine, it cannot be used as a reference for
	present-day ABC code.

- For an overview of all ABC instructions, search for
	`put_instructions_in_table` in
	[`cginput.c`](https://gitlab.science.ru.nl/clean-compiler-and-rts/code-generator/blob/master/cginput.c).
	The instructions are implemented in `cgcode.c` and `cginstructions.c` of the
	same repository. However, the implementation is at times hard to read and
	sometimes it is easier to look at the implementation in the ABC interpreter
	described below.

- [Cloogle](https://cloogle.org) has documentation for some ABC instructions.
	The documentation is generated from
	[`backend/Builtin/ABC.icl`](https://gitlab.com/cloogle/cloogle.org/blob/master/backend/Builtin/ABC.icl).
	There is a [`genabcdoc`](https://gitlab.com/cloogle/cloogle.org/blob/master/util/genabcdoc.icl)
	tool that can generate all ABC documentation in one webpage. Pull requests
	for missing instructions are very welcome.

- There is an
	[ABC interpreter](https://gitlab.com/clean-and-itasks/abc-interpreter).
	The implementation of the instructions can be found in
	`/tools/interpretergen.icl`. This is probably the most useful reference for
	the semantics of ABC instructions. This DSL can currently be used to generate
	C and WebAssembly implementations of the ABC instructions.

	The repository also includes an interactive graphical debugger for the ABC
	machine, inspired by `gdb`. This is the `debug` program found in `/src`. You
	first need to generate bytecode from the ABC code using the workflow
	described in `doc/tools.md`. For help on the interface of `debug`, press
	<kbd>?</kbd> when you are in the GUI.

	For performance reasons, this interpreter has a larger instruction set than
	the actual ABC machine. You can optimise 'common' ABC code to 'optimised' ABC
	code using the `abcopt` tool in the same repository. The interpreter should
	however also work with unoptimised ABC code.

- If you get stuck, ask the maintainer of the
	[ABC interpreter](https://gitlab.com/clean-and-itasks/abc-interpreter)
	or the
	[code generator](https://gitlab.science.ru.nl/clean-compiler-and-rts/code-generator)
	for help.
