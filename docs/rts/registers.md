---
vim: noexpandtab tabstop=2 shiftwidth=2
---

# Processor registers

Here we describe how parts of the [ABC machine](introduction.md) are mapped
onto an actual processor.

- The program is stored in the code and data segment.
- The heap is stored in memory.
- The A stack is stored in memory.
- The B and C stack are intertwined on the machine stack.

Registers are used for the following purposes:

- The machine instruction pointer is also the ABC instruction pointer.
- The machine stack pointer is also the B/C stack pointer.
- One register for the A stack pointer.
- One register to hold a pointer to the unused part of the heap, to write new
	nodes to.
- On some systems, one register to hold the number of free words on the heap.
- Some registers to store the top values on the A and B stacks, for efficiency.
- One or more scratch registers.
- In the run time system (in particular in the garbage collector), registers
	are used for other purposes as well.

Below are mapping tables for various platforms.

The tables include the functions of the registers in common calling
conventions. This is useful when you call an external function not through the
`ccall` ABC instruction but using an ABC `jmp`, `jsr`, etc. This allows you to
efficiently pass the heap and stack pointers to the external function. When
unspecified, a register is caller-saved. See Wikipedia for more details about
these calling conventions:

- [X86 calling conventions](https://en.wikipedia.org/wiki/X86_calling_conventions)

## x86

| Function          | Register | Function in cdecl |
| ----------------- | -------- | ----------------- |
| A stack pointer   | `esi`    | Callee-saved      |
| B/C stack pointer | `esp`    | Stack pointer     |
| Heap pointer      | `edi`    | Callee-saved      |
|                   |          |                   |
| `A[0]`            | `ecx`    |                   |
| `A[1]`            | `edx`    |                   |
|                   |          |                   |
| `B[0]`            | `eax`    | Result            |
| `B[1]`            | `ebx`    | Callee-saved      |
|                   |          |                   |
| Scratch           | `ebp`    | Base pointer      |

(In cdecl, all arguments are passed on the stack.)

## x86-64

| Function          | Register | In System V ABI      | In Windows calling convention |
| ----------------- | -------- | -------------------- | ----------------------------- |
| A stack pointer   | `rsi`    | Argument 2           | Callee-saved                  |
| B/C stack pointer | `rsp`    | Stack pointer        | Stack pointer                 |
| Heap pointer      | `rdi`    | Argument 1           | Callee-saved                  |
| Free words        | `r15`    | Callee-saved         | Callee-saved                  |
|                   |          |                      |                               |
| `A[0]`            | `rcx`    | Argument 4           | Argument 1                    |
| `A[1]`            | `rdx`    | Argument 3; result 2 | Argument 2                    |
| `A[2]`            | `r8`     | Argument 5           | Argument 3                    |
|                   |          |                      |                               |
| `B[0]`            | `rax`    | Result 1             | Result                        |
| `B[1]`            | `rbx`    | Callee-saved         | Callee-saved                  |
| `B[2]`            | `r10`    |                      |                               |
| `B[3]`            | `r11`    |                      |                               |
| `B[4]`            | `r12`    | Callee-saved         | Callee-saved                  |
| `B[5]`            | `r13`    | Callee-saved         | Callee-saved                  |
| `B[6]`            | `r14`    | Callee-saved         | Callee-saved                  |
|                   |          |                      |                               |
| Scratch           | `rbp`    | Base pointer         | Base pointer                  |
| Scratch           | `r9`     | Argument 6           | Argument 4                    |
