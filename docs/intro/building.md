---
vim: noexpandtab tabstop=2 shiftwidth=2
---

# Building

Building Clean programs is done using the nitrile package manager, see https://clean-and-itasks.gitlab.io/nitrile/intro/getting-started/.
