---
vim: noexpandtab tabstop=2 shiftwidth=2
---

# Getting started

See https://clean-and-itasks.gitlab.io/nitrile/intro/getting-started/.
