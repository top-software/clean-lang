---
vim: noexpandtab tabstop=2 shiftwidth=2
---

# Setting up

These are some resources that may be useful for people starting to work with
[Clean](https://clean-lang.org).

## Distributions

- [Nitrile](https://clean-lang.org): the modern clean distribution making use of the nitrile package manager.
- [ftp.cs.ru.nl/Clean/Clean30](https://ftp.cs.ru.nl/Clean/Clean30): the
	official Clean 3.0 distribution
- [ftp.cs.ru.nl/Clean/builds](https://ftp.cs.ru.nl/Clean/builds): the official
	nightlies
- [camilstaps/clean](https://hub.docker.com/r/camilstaps/clean): Docker images
- [packages.altlinux.org/en/sisyphus/srpms/clean](https://packages.altlinux.org/en/sisyphus/srpms/clean):
	ALT Linux package

## Editor plugins

- [Vim](https://gitlab.com/clean-and-itasks/vim-clean)
- [Visual Studio Code](https://github.com/W95Psp/CleanForVSCode)
- [Atom](https://github.com/timjs/atom-language-clean)
- [Eclipse](https://gitlab.science.ru.nl/clean-and-itasks/contrib/clide)
- [Sublime Text 3](https://github.com/matheusamazonas/sublime3_clean)
- [Sublime Text 3 Cloogle integration](https://github.com/hayeb/SublimeCloogle)

## Tools

- [Cloogle](https://cloogle.org/): search engine
- [Ctags generator](https://gitlab.com/cloogle/periphery/cloogle-tags)
- [Online sandbox](https://camilstaps.gitlab.io/clean-sandbox/)

## Syntax highlighters

- [JavaScript (highlight.js)](https://www.npmjs.com/package/highlight.js)
- [JavaScript (standalone)](https://www.npmjs.com/package/clean-highlighter)
- [Python (pygments)](https://pypi.python.org/pypi/pygments-lexer-clean)

## Official repositories

- [clean-and-itasks](https://gitlab.com/clean-and-itasks): Clean and
	iTasks group with libraries
- [clean-compiler-and-rts](https://gitlab.science.ru.nl/clean-compiler-and-rts):
	Radboud GitLab group with the compiler, code generator, and other essentials
- [clean-dynamic-system](https://svn.cs.ru.nl/repos/clean-dynamic-system): the
	dynamics linker and other related tools and libraries
- [clean-libraries](https://svn.cs.ru.nl/repos/clean-libraries): many old
	libraries (including old versions of projects now tracked on GitLab)
- [clean-tools](https://svn.cs.ru.nl/repos/clean-tools): `clm` (now tracked on
	GitLab), an ELF linker (unused), htoclean
