---
vim: noexpandtab tabstop=2 shiftwidth=2
---

# The Clean programming language

This website contains a collection of guides and hints to work with the
[Clean][] programming language.

Articles are added as needed, and as such there is no claim for completeness.
The information here is intended as a less official, more dynamic supplement to
the [official Wiki][Clean] and the language report included in the
distributions.

[![CC-BY-SA 4.0 license](https://i.creativecommons.org/l/by-sa/4.0/88x31.png)](http://creativecommons.org/licenses/by-sa/4.0/)  
This work is licensed under a [Creative Commons Attribution-ShareAlike 4.0 International License](http://creativecommons.org/licenses/by-sa/4.0/).

For the authors, and information about contributing, see [here](misc/about).

[Clean]: https://clean.cs.ru.nl
