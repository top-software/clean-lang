---
vim: noexpandtab tabstop=2 shiftwidth=2
---

# Introduction

The Clean run time system supports different methods of profiling. The
different methods are mutually exclusive. They can be enabled in `clm` and
`cpm` in the following ways:

| Method                                      | `clm` option | `cpm` settings |
| ------------------------------------------- | ------------ | -------------- |
| [Default](#default)                         | (none)       | `Application/Profile/Time: False`<br>`Application/Profile/Callgraph: False`<br>`Application/Profile/Stack: False` |
| [Stack tracing](#stack-tracing)             | `-tst`       | `Application/Profile/Time: True` <br>`Application/Profile/Callgraph: False`<br>`Application/Profile/Stack: True`  |
| [Time profiling](#time-profiling)           | `-pt`        | `Application/Profile/Time: True` <br>`Application/Profile/Callgraph: False`<br>`Application/Profile/Stack: False` |
| [Callgraph profiling](#callgraph-profiling) | `-pg`        | `Application/Profile/Time: False`<br>`Application/Profile/Callgraph: True` <br>`Application/Profile/Stack: False` |
| [Memory profiling](#memory-profiling)       | (n/a)        | `Application/Profile/Memory: True`<br> |

## Default

By default, no profiling is performed.

## Stack tracing

When compiled for stack tracing, the executable does not create a profile
report but will keep track of the call stack and print the top of the stack
when the program crashes.

A stack trace is also produced when time profiling or callgraph profiling is
enabled.

## Time profiling

When compiled for time profiling, the executable tracks for each function:

- The time spent in the function (excluding time spent in functions called by
  this function).
- The number of words allocated by the function (excluding words allocated in
  functions called by this function).
- The number of times it is called in a strict context (i.e., the result is
  immediately used by the caller).
- The number of times it is called in a lazy context (i.e., a thunk is created
  for the result because it may not have to be evaluated).
- The number of times it is called in a curried context (i.e., a closure was
  created containing some arguments [e.g. `((==) x)`], and this closure was
  applied to the remaining arguments).

On exit, this information is written to a file `EXECUTABLE Time Profile.tcl`,
where `EXECUTABLE` is the name of the executable. This file can be viewed in
the Clean IDE or [converted to the callgrind format][cleanprof2callgrind] which
can be viewed in a program like [KCacheGrind][]. Of these methods only the
Clean IDE attempts to correct the time spent in functions for the overhead of
the profiling code.

## Callgraph profiling

An important drawback of [time profiling](#time-profiling) is that it is not
possible to see from where functions are called, or what functions are called
from some function. With callgraph profiling, the entire call tree is tracked.
The same five data points as with time profiling are collected (time spent,
allocated words, strict/lazy/curried calls), but these are separated per call
stack snapshot instead of per function (without information about its caller
context).

On exit, the information is written to a file `EXECUTABLE.pgcl`, where
`EXECUTABLE` is the name of the executable. This file can be converted with
[`convertprofile`][convertprofile] to a HTML file to inspect the tree or a
callgrind output file that can be viewed in [KCacheGrind][].

For the implementation details, see [Callgraph profiling](callgraph.md).

## Memory profiling

(no documentation yet)

[cleanprof2callgrind]: https://gitlab.science.ru.nl/clean-and-itasks/contrib/cleanprof2callgrind
[convertprofile]: https://gitlab.com/camilstaps/clean-convertprofile
[KCacheGrind]: https://kcachegrind.github.io/
