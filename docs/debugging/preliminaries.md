---
vim: noexpandtab tabstop=2 shiftwidth=2
---

# Preliminaries

These pages give some tips to track down crashes and bugs in Clean programs.

It is impossible to give a standardized manual to solve bugs. Apart from the
preliminaries below, the other subsections here are not meant to be read
sequentially. They give different tips to get more information about the bug,
and can be used simultaneously in any combination depending on the nature of
the bug.

First of all, reduce the program as much as easily possible. In particular,
take care to remove sources of randomness and interaction, so that you can
reliably and automatically reproduce the bug. Also try to remove `fork`s.

Secondly, take the following steps to ensure your program has as much debug
information as possible:

- Compile with exporting labels (in nitrile.yml, set `clm_options.export_local_labels:true`).
- Compile with stack tracing (in nitrile.yml, set `clm_options.profiling:StackTracing`).
- Disable ABC optimization (in nitrile.yml, set bytecode: {optimize_abc: false, prelink: true or false}).
- If you suspect problems in underlying C code, be sure to compile with debug
	information (`-g`), so `CFLAGS="-g" make ...`.

For some of these settings, different code has to be generated. This may cause
the bug to disappear. In that case you will have to make do with less debug
information.
