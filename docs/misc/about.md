---
vim: noexpandtab tabstop=2 shiftwidth=2
---

# About these pages

## Contributing

The source for these pages can be found on
[GitLab](https://gitlab.com/top-software/clean-lang).
Contribution is much appreciated.

## Authors

- Elroy Jumpertz
- Camil Staps
- Gijs Alberts

## License

[![CC-BY-SA 4.0 license](https://i.creativecommons.org/l/by-sa/4.0/88x31.png)](http://creativecommons.org/licenses/by-sa/4.0/)
This work is licensed under a [Creative Commons Attribution-ShareAlike 4.0 International License](http://creativecommons.org/licenses/by-sa/4.0/).
